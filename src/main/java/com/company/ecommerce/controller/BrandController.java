package com.company.ecommerce.controller;

import com.company.ecommerce.entity.Brand;
import com.company.ecommerce.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/brand")
public class BrandController {
    private final BrandService brandService;

    @GetMapping
    public List<com.company.ecommerce.requestDto.Brand> categories() {
        return brandService.findAll();
    }

    @PostMapping
    public String saveCategory(@RequestBody Brand brand) {
        return brandService.saveBrand(brand);
    }

    @GetMapping(value = "/{id}")
    public Optional<com.company.ecommerce.requestDto.Brand> findById(@PathVariable Long id) {
        return brandService.findById(id);
    }

    @PutMapping()
    public String updateProduct(@RequestBody Brand brand) {
        return brandService.updateBrand(brand);
    }

}
