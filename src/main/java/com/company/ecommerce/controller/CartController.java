package com.company.ecommerce.controller;

import com.company.ecommerce.entity.Cart;
import com.company.ecommerce.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/cart")
public class CartController {
    private final CartService cartService;

    @GetMapping
    public List<Cart> carts() {
        return cartService.findAll();
    }

    @PostMapping
    public String saveCart(@RequestBody Cart cart) {
        return cartService.saveCart(cart);
    }

    @GetMapping(value = "/{id}")
    public Optional<Cart> findById(@PathVariable Long id) {
        return cartService.findById(id);
    }

    @PutMapping()
    public String updateCart(@RequestBody Cart cart) {
        return cartService.updateCart(cart);
    }

}
