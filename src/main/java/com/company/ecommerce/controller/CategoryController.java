package com.company.ecommerce.controller;

import com.company.ecommerce.entity.Category;
import com.company.ecommerce.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/category")
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping
    public List<Category> categories() {
        return categoryService.findAll();
    }

    @PostMapping
    public String saveCategory(@RequestBody Category category) {
        return categoryService.saveCategory(category);
    }

    @GetMapping(value = "/{id}")
    public Optional<Category> findById(@PathVariable Long id) {
        return categoryService.findById(id);
    }

    @PutMapping()
    public String updateProduct(@RequestBody Category category){
        return categoryService.updateCategory(category);
    }

}
