package com.company.ecommerce.controller;

import com.company.ecommerce.entity.Product;
import com.company.ecommerce.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/product")
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public List<Product> products() {
        return productService.findAll();
    }

    @PostMapping
    public String saveProduct(@RequestBody Product product) {
        return productService.saveProduct(product);
    }

    @GetMapping(value = "/{id}")
    public Optional<Product> findById(@PathVariable Long id) {
        return productService.findById(id);
    }

    @PutMapping()
    public String updateProduct(@RequestBody Product product) {
        return productService.updateProduct(product);
    }

}
