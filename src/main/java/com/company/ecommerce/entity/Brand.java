package com.company.ecommerce.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String desccc;

    @OneToMany(mappedBy = "brand",cascade = CascadeType.PERSIST)
    List<Product> products;

    @ManyToMany(cascade = CascadeType.PERSIST)
    List<Category> categories;
}
