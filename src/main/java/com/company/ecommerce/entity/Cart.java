package com.company.ecommerce.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long count;
    Double total_amount;

    @OneToMany(mappedBy = "cart",cascade = CascadeType.PERSIST)
    List<Product> product;

    @OneToOne(cascade = CascadeType.PERSIST)
    User user;
}
