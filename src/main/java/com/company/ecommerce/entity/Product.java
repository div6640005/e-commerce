package com.company.ecommerce.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String desccc;
    Double amount;
    Long remainCount;

    @ManyToOne(cascade = CascadeType.PERSIST)
    Brand brand;

    @ManyToOne(cascade = CascadeType.PERSIST)
    Cart cart;

    @Embedded
    ProductDetails productDetails;
}
