package com.company.ecommerce.requestDto;

import com.company.ecommerce.entity.Category;
import com.company.ecommerce.entity.Product;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Brand {
    Long id;
    String name;
    String desccc;

    List<com.company.ecommerce.requestDto.Product> products;

    List<com.company.ecommerce.requestDto.Category> categories;
}
