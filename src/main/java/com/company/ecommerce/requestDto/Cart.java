package com.company.ecommerce.requestDto;

import com.company.ecommerce.entity.Product;
import com.company.ecommerce.entity.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Cart {
    Long id;
    Long count;
    Double total_amount;

    List<com.company.ecommerce.requestDto.Product> product;

    com.company.ecommerce.requestDto.User user;
}
