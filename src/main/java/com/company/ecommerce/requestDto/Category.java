package com.company.ecommerce.requestDto;

import com.company.ecommerce.entity.Brand;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Category {
    Long id;
    String name;
    String desccc;

    List<com.company.ecommerce.requestDto.Brand> brands;
}
