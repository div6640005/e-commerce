package com.company.ecommerce.requestDto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product {
    Long id;
    String name;
    String desccc;
    Double amount;
    Long remainCount;

    com.company.ecommerce.requestDto.Brand brand;

    com.company.ecommerce.requestDto.Cart cart;

}
