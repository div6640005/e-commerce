package com.company.ecommerce.requestDto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {
    Long id;
    String name;
    LocalDate birthdate;
    String email;
    String address;

    com.company.ecommerce.requestDto.Cart cart;

}
