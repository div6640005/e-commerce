package com.company.ecommerce.service;

import com.company.ecommerce.entity.Brand;

import java.util.List;
import java.util.Optional;


public interface BrandService {
    List<com.company.ecommerce.requestDto.Brand> findAll();
    Optional<com.company.ecommerce.requestDto.Brand> findById(Long id);

    String saveBrand(Brand brand);

    String updateBrand(Brand brand);

    String deleteBrand(Long id);
}
