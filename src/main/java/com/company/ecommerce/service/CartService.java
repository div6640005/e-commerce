package com.company.ecommerce.service;

import com.company.ecommerce.entity.Brand;
import com.company.ecommerce.entity.Cart;

import java.util.List;
import java.util.Optional;

public interface CartService {
    List<Cart> findAll();
    Optional<Cart> findById(Long id);

    String saveCart(Cart cart);

    String updateCart(Cart cart);

    String deleteCart(Long id);
}
