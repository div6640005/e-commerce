package com.company.ecommerce.service;

import com.company.ecommerce.entity.Brand;
import com.company.ecommerce.entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> findAll();
    Optional<Category> findById(Long id);

    String saveCategory(Category category);

    String updateCategory(Category category);

    String deleteCategory(Long id);
}
