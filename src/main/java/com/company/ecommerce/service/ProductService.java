package com.company.ecommerce.service;

import com.company.ecommerce.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> findAll();

    Optional<Product> findById(Long id);

    String saveProduct(Product product);

    String updateProduct(Product product);

    String deleteProduct(Long id);
}
