package com.company.ecommerce.service.impl;

import com.company.ecommerce.Configuration.Config;
import com.company.ecommerce.entity.Brand;
import com.company.ecommerce.repository.BrandRepository;
import com.company.ecommerce.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {
    private final Config config;
    private final BrandRepository brandRepository;

    @Override
    public List<com.company.ecommerce.requestDto.Brand> findAll() {
        List<com.company.ecommerce.requestDto.Brand> brandList = new ArrayList<>();
        config.modelMapper().map(brandRepository.findAll(), brandList);

        return brandList;
    }

    @Override
    public Optional<com.company.ecommerce.requestDto.Brand> findById(Long id) {
        Optional<com.company.ecommerce.requestDto.Brand> brandOptional = null;
        config.modelMapper().map(brandRepository.findById(id),brandOptional);

        return brandOptional;
    }

    @Override
    public String saveBrand(Brand brand) {
        brandRepository.save(brand);
        return "Save successfully";
    }

    @Override
    public String updateBrand(Brand brand) {
        brandRepository.save(brand);
        return "Update successfully";
    }

    @Override
    public String deleteBrand(Long id) {
        brandRepository.deleteById(id);
        return "Delete successfully";
    }
}
