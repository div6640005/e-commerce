package com.company.ecommerce.service.impl;

import com.company.ecommerce.entity.Cart;
import com.company.ecommerce.repository.CartRepository;
import com.company.ecommerce.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CartServiceImpl implements CartService {
    private final CartRepository cartRepository;

    @Override
    public List<Cart> findAll() {
        return cartRepository.findAll();
    }

    @Override
    public Optional<Cart> findById(Long id) {
        return cartRepository.findById(id);
    }

    @Override
    public String saveCart(Cart cart) {
        cartRepository.save(cart);
        return "Save Successfully";
    }

    @Override
    public String updateCart(Cart cart) {
        cartRepository.save(cart);
        return "Update Successfully";
    }

    @Override
    public String deleteCart(Long id) {
        cartRepository.deleteById(id);
        return "Delete Successfully";
    }
}
