package com.company.ecommerce.service.impl;

import com.company.ecommerce.entity.Category;
import com.company.ecommerce.repository.CategoryRepository;
import com.company.ecommerce.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public String saveCategory(Category category) {
        categoryRepository.save(category);
        return "Save successfully";
    }

    @Override
    public String updateCategory(Category category) {
        categoryRepository.save(category);
        return "Update successfully";
    }

    @Override
    public String deleteCategory(Long id) {
        categoryRepository.deleteById(id);
        return "Delete successfully";
    }
}
