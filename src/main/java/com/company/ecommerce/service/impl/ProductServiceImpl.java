package com.company.ecommerce.service.impl;

import com.company.ecommerce.entity.Product;
import com.company.ecommerce.repository.ProductRepository;
import com.company.ecommerce.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public String saveProduct(Product product) {
        productRepository.save(product);
        return "Save successfully";
    }

    @Override
    public String updateProduct(Product product) {
        productRepository.save(product);
        return "Update successfully";
    }

    @Override
    public String deleteProduct(Long id) {
        productRepository.deleteById(id);
        return "Delete successfully";
    }
}
